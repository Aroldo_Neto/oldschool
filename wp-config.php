<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'oldschool');

/** MySQL database username */
define('DB_USER', 'oldschool');

/** MySQL database password */
define('DB_PASSWORD', '0ld5ch00l');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'em!mSR|{$BAckC=o*xf!s}Yfc5T+2|06PB;{r%rFmCGdFR.%M}_WXP5UqZ%j`Fm/');
define('SECURE_AUTH_KEY',  'M|I<vEU4l3E|Z4I|}nNn.CZJu6Yzm -$yfd0 wb8phPix-F_KAJ&+RK5|uR5lD]Z');
define('LOGGED_IN_KEY',    '|}QSl-z=h0)PFIs|]]lF!QE 3T@r%)N-X6MkZ4=)$;cU]_{w=J` O}V+*<^0|CL;');
define('NONCE_KEY',        'FEZ<1~,B;I?]!eja4>>3+fp>.> {};*TtW$f27,owdWJ&@tc|Y-d|$O1_nMzHV,|');
define('AUTH_SALT',        ':K8VtFhFVXv&yJh?,u9/B]$e^x:t)}9uw]be|(a~|!|oq.JCP>|ndSVZW|rZ)+)Y');
define('SECURE_AUTH_SALT', '>|d<zM/?>-diQk|_QGL#tC!dR&Ry.xj1Ov3/N,p4%]h4^/1?-zGu:yrXU|UvkkN*');
define('LOGGED_IN_SALT',   'yW>Wyb c~*2JDHT/2fb4<g1hSOa-&cb*bUs0U$uNXJgqiMz0+NB[ujS11<-tn05n');
define('NONCE_SALT',       '7msHeT^uqqpIk441I-7=JK)y2QEk}}]`v)/rj|:,E2#,cYMtF46i(8i^:yj/7(gB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
